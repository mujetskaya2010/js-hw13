/// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

/// setTimeout дозволяє викликати функцію один раз через певний промідок часу, 
/// а setInterval викликає функцію регулярно, повторюючи виклик через певний проміжок часу.

/// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

/// Планування з нульовою затримкою setTimeout(func,0) робить виклик якнайшвидше, але після завершення виконання поточного коду.

/// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

/// Функція посилається на зовнішнє середовище, воно може зайняти набагато більше памяті, ніж сама функція.
/// Тому, коли запланована функція більше не потрібна, краще її скасувати, навіть якщо вона дуже мала.



const allImg = Array.from(document.querySelectorAll("img"));
const playBtn = document.querySelector(".play");
const stopBtn = document.querySelector(".stop");

let showImg;
let nextIndex = 0;

const show_img = () => {
  for (let i = 0; i < allImg.length; i++) {
    if (i === nextIndex) {
      allImg[i].classList.add("show");
      allImg[i].classList.remove("hidden");
    } else {
      allImg[i].classList.remove("show");
      allImg[i].classList.add("hidden");
    }
  }

  ++nextIndex;

  if (nextIndex >= allImg.length) {
    nextIndex = 0;
  }
}

stopBtn.addEventListener("click", () => {
  clearInterval(showImg);

  stopBtn.disabled = true;
  playBtn.disabled = false;
});

playBtn.addEventListener("click", () => {
  showImg = setInterval(show_img, 3000);

  playBtn.disabled = true;
  stopBtn.disabled = false;
});

playBtn.click();
